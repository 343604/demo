#DEMO PROJECT#

##Demonstration
http://dm.salamakhin.ru/

##Installing
Clone repo
```
cd {projectPath};
git clone git@bitbucket.org:343604/demo.git
cd demo;
```

Setup config
```
cp app/config/parameters.yml.dist app/config/parameters.yml;
vi app/config/parameters.yml;
```

Clear caches
```
php app/console cache:clear --env=prod;
```

Setup write permissions,selinux:
If selinux is on, set policy httpd_sys_rw_content_t to app/logs, app/cache!
```
chmod 777 app/cache -R;
chmod 777 app/logs -R;
```

Build bootstrap
```
php ./vendor/sensio/distribution-bundle/Resources/bin/build_bootstrap.php;
```

Create DB structure
```
php app/console doctrine:migrations:migrate --no-interaction;
```

Insert test sql data:
```
mysql -p -u {user} {dbname} < src/AppBundle/Resources/Dumps/testData.sql;
```

Configure web-server entry point
```
web/app.php
```