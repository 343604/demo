<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161102103916 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE languages (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE descriptions (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, info LONGTEXT NOT NULL, vacancyId INT NOT NULL, languageId INT NOT NULL, INDEX IDX_C96EAEB63FD21740 (vacancyId), INDEX IDX_C96EAEB6940D8C7E (languageId), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE departments (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vacancies (id INT AUTO_INCREMENT NOT NULL, departmentId INT NOT NULL, INDEX IDX_99165A593CDC2CC0 (departmentId), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE descriptions ADD CONSTRAINT FK_C96EAEB63FD21740 FOREIGN KEY (vacancyId) REFERENCES vacancies (id)');
        $this->addSql('ALTER TABLE descriptions ADD CONSTRAINT FK_C96EAEB6940D8C7E FOREIGN KEY (languageId) REFERENCES languages (id)');
        $this->addSql('ALTER TABLE vacancies ADD CONSTRAINT FK_99165A593CDC2CC0 FOREIGN KEY (departmentId) REFERENCES departments (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE descriptions DROP FOREIGN KEY FK_C96EAEB6940D8C7E');
        $this->addSql('ALTER TABLE vacancies DROP FOREIGN KEY FK_99165A593CDC2CC0');
        $this->addSql('ALTER TABLE descriptions DROP FOREIGN KEY FK_C96EAEB63FD21740');
        $this->addSql('DROP TABLE languages');
        $this->addSql('DROP TABLE descriptions');
        $this->addSql('DROP TABLE departments');
        $this->addSql('DROP TABLE vacancies');
    }
}
