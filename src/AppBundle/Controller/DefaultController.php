<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * Mainpage
     * 
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
		$languageIdSelected = empty($_COOKIE['lang']) ? 1 : (int)$_COOKIE['lang'];
		$languages = (array)$this->getDoctrine()->getRepository('AppBundle:Language')->findAll();
		foreach($languages as &$language){
			$language = $language->toArray();
			$language['selected'] = $language['id'] == $languageIdSelected ? 1 : 0;
		}
		
		$departments = (array)$this->getDoctrine()->getRepository('AppBundle:Department')->findAll();
		foreach($departments as &$department)
			$department = $department->toArray();

		return $this->render('default/index.html.twig',[
			'version' => '011120161',
			'languages' => $languages,
			'departments' => $departments,
		]);
    }
}
