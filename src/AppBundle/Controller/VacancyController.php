<?php
namespace AppBundle\Controller;

use FOS\RestBundle\Controller\Annotations\Get;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class VacancyController extends Controller{
	/**
	 * Find vacancies from DB
	 * 
	 * @Get("/vacancies")
	 * @Get("/vacancies/{departmentId}")
	 * @return JsonResponse
	 */
	public function getVacanciesAction($departmentId){
		$departmentId = (int)$departmentId;
		$languageId = empty($_COOKIE['lang']) ? 1 : (int)$_COOKIE['lang'];
		
		$repo = $this->getDoctrine()->getRepository('AppBundle:Vacancy');
		$vacancies = $repo->getVacancies($departmentId,$languageId);
		
		$code = $vacancies ? 200 : 404;
		
		return new JsonResponse($vacancies,$code);
	}
}