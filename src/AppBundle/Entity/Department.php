<?php
namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\Table(name="departments")
 */
class Department
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(type="string")
     */
    private $name;
    /**
     * @ORM\OneToMany(targetEntity="Vacancy",mappedBy="departments")
     */
    private $vacancies;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->vacancies = new \Doctrine\Common\Collections\ArrayCollection();
    }

	public function toArray(){
		return [
			'id' => $this->id,
			'name' => $this->name,
		];
	}


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Department
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add vacancies
     *
     * @param \AppBundle\Entity\Vacancy $vacancies
     * @return Department
     */
    public function addVacancy(\AppBundle\Entity\Vacancy $vacancies)
    {
        $this->vacancies[] = $vacancies;

        return $this;
    }

    /**
     * Remove vacancies
     *
     * @param \AppBundle\Entity\Vacancy $vacancies
     */
    public function removeVacancy(\AppBundle\Entity\Vacancy $vacancies)
    {
        $this->vacancies->removeElement($vacancies);
    }

    /**
     * Get vacancies
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVacancies()
    {
        return $this->vacancies;
    }
}
