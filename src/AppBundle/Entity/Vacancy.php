<?php
namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\VacancyRepository")
 * @ORM\Table(name="vacancies")
 */
class Vacancy
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\OneToMany(targetEntity="Description",mappedBy="vacancies")
     */
    private $descriptions;
    /**
     * @ORM\ManyToOne(targetEntity="Department",inversedBy="vacancies")
     * @ORM\JoinColumn(name="departmentId",referencedColumnName="id",nullable=false)
     */
    private $department;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->descriptions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add descriptions
     *
     * @param \AppBundle\Entity\Description $descriptions
     * @return Vacancy
     */
    public function addDescription(\AppBundle\Entity\Description $descriptions)
    {
        $this->descriptions[] = $descriptions;

        return $this;
    }

    /**
     * Remove descriptions
     *
     * @param \AppBundle\Entity\Description $descriptions
     */
    public function removeDescription(\AppBundle\Entity\Description $descriptions)
    {
        $this->descriptions->removeElement($descriptions);
    }

    /**
     * Get descriptions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDescriptions()
    {
        return $this->descriptions;
    }

    /**
     * Set department
     *
     * @param \AppBundle\Entity\Department $department
     * @return Vacancy
     */
    public function setDepartment(\AppBundle\Entity\Department $department = null)
    {
        $this->department = $department;

        return $this;
    }

    /**
     * Get department
     *
     * @return \AppBundle\Entity\Department 
     */
    public function getDepartment()
    {
        return $this->department;
    }
}
