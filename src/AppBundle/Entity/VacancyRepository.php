<?php
namespace AppBundle\Entity;
use Doctrine\ORM\EntityRepository;
class VacancyRepository extends EntityRepository{
	/**
	 * Find vacancies from DB
	 * 
	 * @param int $departmentId
	 * @param int $languageId
	 * @return array
	 */
	public function getVacancies($departmentId,$languageId){
		$select = [
			'dp.name AS deptName',
			'd.name','d.info'
		];
		
		$builder = $this->getEntityManager()->getRepository('AppBundle:Vacancy')
			->createQueryBuilder('v')
			->innerJoin('v.department', 'dp')
			->innerJoin('AppBundle:Description', 'd', 'WITH', 'd.vacancy = v.id AND d.language = 1');
			
		if($languageId > 1){
			$builder
				->leftJoin('AppBundle:Description', 'dd', 'WITH', 'dd.vacancy = v.id AND dd.language = :languageId')
				->setParameter('languageId', $languageId);
			$select[] = 'dd.name AS lName';
			$select[] = 'dd.info AS lInfo';
		}
			
		if($departmentId)$builder
			->where('dp.id = :departmentId')
			->setParameter('departmentId', $departmentId);
			
		$vacancies = $builder->select($select)->getQuery()->getResult();
		
		foreach($vacancies as &$vacancy)
			$vacancy = [
				'deptName' => $vacancy['deptName'],
				'name' => empty($vacancy['lName']) ? $vacancy['name'] : $vacancy['lName'],
				'info' => empty($vacancy['lInfo']) ? $vacancy['info'] : $vacancy['lInfo'],
			];
		
		return $vacancies;
	}
}