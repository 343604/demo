$(function(){
	$('.languages>div').on('click',function(){
		app.language.set(this);
	});
	$('.departments>select').on('change',function(){
		app.department.set(this);
	});
	app.vacancy.find();
});
var app = {
	vacancy:{
		process:false,
		find:function(){
			$('.result').html('loading');
			if(this.process)this.process.abort();
			var url = 'vacancies/'+$('.departments>select').val();
			this.process = $.ajax({url:url,type:'GET',cache:false,success:function(data,textStatus,request){
				var html = '';
				for(i in data){
					var vacancy = data[i];
					html += '<div class="vacancy">Department: '+vacancy.deptName+'<br>Name: '+vacancy.name+'<br>'+vacancy.info+'</div>';
				}
				$('.result').html(html);
			},error:function(){
				$('.result').html('not found');
			}});
		}
	},
	department:{
		set:function(el){
			app.vacancy.find();
		}
	},
	language:{
		set:function(el){
			app.cookie.set('lang',$(el).data('id'));
			$('.languages>div').removeClass('selected1');
			$(el).addClass('selected1');
			app.vacancy.find();
		}
	},
	cookie:{
		set:function(name, value, options){
			options = options || {expires:60*60*24*30*12};
			var expires = options.expires;
			if (typeof expires == "number" && expires) {
				var d = new Date();
				d.setTime(d.getTime() + expires * 1000);
				expires = options.expires = d;
			}
			if(expires && expires.toUTCString)options.expires = expires.toUTCString();
			value = encodeURIComponent(value);
			var updatedCookie = name + "=" + value;
			for(var propName in options){
				updatedCookie += "; " + propName;
				var propValue = options[propName];
				if(propValue !== true)updatedCookie += "=" + propValue;
			}
			document.cookie = updatedCookie;
		}
	}
};